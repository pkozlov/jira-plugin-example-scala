package com.example

import com.atlassian.sal.api.ApplicationProperties

class MyPluginComponentImpl(val props: ApplicationProperties) extends MyPluginComponent {
  override def getName = if (props != null) s"myComponent:${props.getDisplayName}" else "myComponent"
}
